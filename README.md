decl.js
=======

---

**decl.js** allows you to create a user interface based on the description of the elements, their behaviors and relations as json.

For more info go to [decl.js home page](http://decljs.kenfin.ru).
