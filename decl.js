/*
	@name decl.js
	@description Module for declarative creating dynamic elements
	@author ikenfin@gmail.com
	@homepage i.kenfin.ru
	@license absolutely free	
 */
(function(global) {

	"use strict";

	global.Decl = function() {

		// context
		var self = this;

		// getter object
		var __getter = {
			// get rendered element by identifier
			'get' : function(id) {
				return __rendered_identifiers[id];
			},
			// get context
			'context' : function() {
				return self;
			}
		};

		// stores rendered elements
		var __rendered_identifiers = {};

		// you can overload this function
		this.render = function(json) {
			return __render_elements(json);
		}

		// декларативное построение элементов.
		// the heart of module - create elements declaratively
		function __render_elements(element) {

			if(element == null || element == undefined) return;

			var _buf = null;

			if(element.tag !== undefined) {

				if(element.options && element.options.id)
					_buf = document.getElementById(element.options.id)

				if(!_buf)
					_buf = document.createElement(element.tag);
				
				if(element.options != undefined) {
					for(var option in element.options) {
						_buf[option] = element.options[option];
					}
				}
				if(element.attributes != undefined) {
					for(var attribute in element.attributes) {
						_buf.setAttribute(attribute, element.attributes[attribute]);
					}
				}
				if(element.text != undefined) {
					_buf.innerText = element.text;
				}

				// назначаем события
				// event assignation
				/*
					callback signature fn(target, getter, event)
					
					target - dom element
					getter - getter object
					event  - browser event
				*/
				if(element.events != undefined) {
					for(var eventName in element.events) {
						
							var callback = function(){},
								target_element = _buf;

							if(element.events[eventName].target_element) {
								target_element = element.events[eventName].target_element;
							}
							
							if(element.events[eventName] instanceof Function) {
								callback = element.events[eventName];
							}
							else callback = element.events[eventName].callback;
							
							_delegate(eventName, _buf, target_element, function(target, e) {
								callback.call(_buf, target, __getter, e);
							});
						
					}
				}

				// recursive childrens rendering
				if(element.childrens !== undefined && element.childrens.length > 0) {
					for(var i = 0; i < element.childrens.length; i++) {
						var rendered = __render_elements(element.childrens[i]);
						if(rendered) _buf.appendChild(rendered);
					}
				}
				// store elements with identifier field
				if(element.identifier !== undefined) {
					__rendered_identifiers[element.identifier] = _buf;
				}

			}

			return _buf;
		}

		// вешаем события на пагинатор
		// event delegation
		function _delegate(eventName, parent, target_element, callback) {
			// DOM1
			parent['on' + eventName] = function(e) {
				e = e || window.event;
				var target = e && e.target || e.srcElement;
				
				if((typeof target_element) == 'string' && target.tagName.toString().toLowerCase() == target_element) {
					callback.call(parent, target, e);
				}
				else if(target_element == parent) {
					callback.call(parent, target, e);
				}

			}
		}

	}

}(window));

